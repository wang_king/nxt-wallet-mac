//
//  NSApplication+Relaunch.h
//  NxtMac
//

#import <Cocoa/Cocoa.h>

#define NSApplicationRelaunchDaemon @"NxtWallet Relauncher"

@interface NSApplication (Relaunch)

- (void)relaunch:(id)sender;

@end