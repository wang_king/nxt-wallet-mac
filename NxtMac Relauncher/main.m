//
//  main.m
//  NxtWallet Relauncher
//
//http://13bold.com/tutorials/relaunching-your-application/

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    BOOL success;

    @autoreleasepool {
        // wait a sec, to be safe
        sleep(1);
     
        NSString *appPath = [NSString stringWithCString:argv[1] encoding:NSUTF8StringEncoding];
        success = [[NSWorkspace sharedWorkspace] openFile:[appPath stringByExpandingTildeInPath]];
    }
    
    return (success) ? 0 : 1;
}

